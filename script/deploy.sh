#!/bin/sh
#
# @file         deploy.sh
#               Push a tag in the working directory, resulting in a deployment.
# @author       Jason Erb, jason@distributive.network
# @date         February 2023

set -e

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <major>.<minor>.<patch>" >&2
  exit 1
fi
TAG=$1

CHANGES=$(git status --porcelain=v1 2>/dev/null)
if [ ! -z "${CHANGES}" ]
then
  echo "${CHANGES}"
  echo "Uncommitted changes; aborting..."
  exit 1
fi

BRANCH=$(git branch --show-current)
if [ ! "${BRANCH}" = "release" ]
then
  echo "On '${BRANCH}' branch instead of 'release'; aborting..."
  exit 1
fi

if [ -f VERSION.txt ]
then
  VERSION_TAG=$(cat VERSION.txt)
  if [ -z "${VERSION_TAG}" ]
  then
    echo "Version not found in 'VERSION.txt'; aborting..."
    exit 1
  fi
else
  echo "No 'VERSION.txt'; using ${TAG}..."
  VERSION_TAG="${TAG}"
fi

git fetch --tags
git tag -a "${TAG}" HEAD -m "Tag ${TAG} for version ${VERSION_TAG}"
git push origin "${BRANCH}" "${TAG}"
