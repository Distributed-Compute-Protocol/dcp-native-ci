# CONTRIBUTING

## Code Guidelines

* Use single-quoted strings by default:
  * Double-quoted strings require escaping of backslashes and other special
    characters; use if necessary.
  * Unquoted strings can fail if starting with special characters; avoid.
* Use `$VARIABLE` instead of `${VARIABLE}` by default (except in scripts, which
  following the coding standards for the script in question):
  * There is at least one case where the latter causes an error and the former
    does not (eg. `- if: $CI_COMMIT_TAG`).
