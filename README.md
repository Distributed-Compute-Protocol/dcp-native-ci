# INTRODUCTION

This repository contains DCP Native CI scripts and templates.

## GitLab

A GitLab script or template file from this repository can be included in a
`".gitlab-ci.yml"` file in another GitLab repository with the following code:
```
include:
  - project: 'Distributed-Compute-Protocol/dcp-native-ci'
    ref: <tag>
    file: '/include/[<subdirectory>/]<file>.yml'
```

## Deploying

Distributive GitLab projects with names ending in "-build" are "build" projects,
responsible for building a "target" project. "Deployment" adds the step of
deploying the resulting binaries to the package registry.

To perform a deployment, navigate to the build project repository root and enter
the following:
```
<dcp-native-ci-repository-path>/script/deploy.sh <build-major>.<build-minor>.<build-patch>
```

The deployment version is distinct from the version of the target it is
building. The deployment version is formulated as follows:

* The major version corresponds to a unique target version. Any target version
  change requires a corresponding deployment major version change, along with
  specifying the new target version in the "VERSION.txt" file in the build
  project.
* The minor version increments for a potentially breaking change to the code in
  the build project (for example, changing a build option).
* The patch version increments for non-breaking changes to the code in the build
  project.

The deploy script will cause a deployment version tag to be pushed, which in
turn will:

* Create a "deploy" phase that uploads zipped build artifacts to the Package
  Registry for the build project (at the "-/packages" path under the build
  project URL).
* Create a manual "release" phase that uploads zipped build artifacts to a
  Release for the build project (at the "-/releases" path under the build
  project URL). Once any testing is complete, the "release" phase job(s) can be
  manually invoked.

The Package Registry page (at the "-/packages" path under the build project
URL) contains public links to the build packages and SHA256 hashes, which can be
used by any clients wishing to download the builds.
